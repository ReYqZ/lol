# Client Base 1.4 #

For people too lazy to make the client base for themselves.

### What is this repository for? ###

This is a simple hacked client base for Minecraft. In it's current state it has plenty of features already set up and all that is left to you is to create some implementations.

#### Current Features ####

* Optifine
* Non-Reflection based event system
* Advanced Keybinding *(Supports key masks. Examples: Control+Y, Shift+R)*
* Secure account management *(Each account is encrypted with a user-specific password)*
* Module manager that supports loading from external jars
* Simple and extensible click GUI

### How do I get set up? ###

#### Minecraft 1.8 (MCP-910) ####

1. Install the latest Java
2. Download Minecraft 1.8.0 from the Minecraft launcher
3. Download MCP 910
4. Extract MCP to a folder of your choice. Run decompile.bat or decompile.sh *(Takes 10 minutes)*
5. Replace the contents of the src folder with the source in this repo

#### Minecraft 1.8.8 - 1.9+ ####

1. Install the latest Java
2. Download Minecraft version of your choice
3. Download the matching version of MCP
4. Extract MCP to a folder of your choice. Run decompile.bat or decompile.sh *(Should take 10 minutes)*
5. Copy over the me package from the repo. The code in this repo is for 1.8.0 and will not work with newer versions. You will have to manually re-add all of the event calls and other optimizations from optifine.

### Relevant Links ###

* [Minecraft Coder Pack (MCP) Releases](http://www.modcoderpack.com/website/releases)


### Halp I get errorz!!! ###

Obligatory "It works on my machine"

In all seriousness MCP has two errors by default. It constantly grabs the wrong versions of Mojang's Authlib and Realms lib. Fix the build order by editing the client's properties in eclipse and you should be good to go. If anything else comes up that's not something simple, PM somebody who you think is at least somewhat intelligent or google.

Fun fact 1: IT is being paid to google people's problems who can't google it themselves.
Fun fact 2: Every time I git commit, I forget to add this file. I retype it every time... 