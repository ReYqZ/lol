package me.lpk.client.util;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;
import net.minecraft.world.chunk.Chunk;

public class PlayerUtil implements MinecraftUtil {
	public static boolean isInLiquid() {
		AxisAlignedBB boundingBox = mc.thePlayer.getEntityBoundingBox();
		if (boundingBox == null){
			return false;
		}
		int x1 = MathHelper.floor_double(boundingBox.minX);
		int x2 = MathHelper.floor_double(boundingBox.maxX + 1.0D);
		int y1 = MathHelper.floor_double(boundingBox.minY);
		int y2 = MathHelper.floor_double(boundingBox.maxY + 1.0D);
		int z1 = MathHelper.floor_double(boundingBox.minZ);
		int z2 = MathHelper.floor_double(boundingBox.maxZ + 1.0D);
		if (mc.theWorld.getChunkFromChunkCoords((int) mc.thePlayer.posX >> 4, (int) mc.thePlayer.posZ >> 4) == null) {
			return false;
		}
		for (int x = x1; x < x2; x++) {
			for (int y = y1; y < y2; y++) {
				for (int z = z1; z < z2; z++) {
					Block block = mc.theWorld.getBlockState((new BlockPos(x, y, z))).getBlock();
					if ((block instanceof BlockLiquid)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean isOnLiquid() {
		AxisAlignedBB boundingBox = mc.thePlayer.getEntityBoundingBox();
		if (boundingBox == null){
			return false;
		}
		boundingBox = boundingBox.contract(0.01D, 0.0D, 0.01D).offset(0.0D, -0.01D, 0.0D);
		boolean onLiquid = false;
		int y = (int) boundingBox.minY;
		for (int x = MathHelper.floor_double(boundingBox.minX); x < MathHelper.floor_double(boundingBox.maxX + 1.0D); x++) {
			for (int z = MathHelper.floor_double(boundingBox.minZ); z < MathHelper.floor_double(boundingBox.maxZ + 1.0D); z++) {
				Block block = mc.theWorld.getBlockState((new BlockPos(x, y, z))).getBlock();
				if (block != Blocks.air) {
					if (!(block instanceof BlockLiquid)) {
						return false;
					}
					onLiquid = true;
				}
			}
		}
		return onLiquid;
	}

	public static boolean isMoving() {
		return Math.abs(mc.thePlayer.motionX) >= 0.01 || Math.abs(mc.thePlayer.motionZ) >= 0.01;
	}
}
