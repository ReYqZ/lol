package me.lpk.client.util.security;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.Key;
import java.util.Enumeration;

import javax.crypto.Cipher;
import org.apache.commons.codec.binary.Base64;

public class Crypto {
	/**
	 * Encrypts text with a given key.
	 * 
	 * @param key
	 * @param text
	 * @throws Exception
	 *             Thrown if the key is invalid
	 */
	public static String encrypt(Key key, String text) throws Exception {
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encrypted = cipher.doFinal(text.getBytes());
		byte[] encryptedValue = Base64.encodeBase64(encrypted);
		return new String(encryptedValue);
	}

	/**
	 * Decrypts text with a given key.
	 * 
	 * @param key
	 * @param text
	 * @return
	 * @throws Exception
	 *             Thrown if the key is invalid
	 */
	public static String decrypt(Key key, String text) throws Exception {
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] decodedBytes = Base64.decodeBase64(text.getBytes());
		byte[] original = cipher.doFinal(decodedBytes);
		return new String(original);
	}

	/**
	 * Creates a unique byte array (for key generation) that is user-specific.
	 * 
	 * @param size
	 * @return
	 */
	public static byte[] getUserKey(int size) {
		byte[] ret = new byte[size];
		for (int i = 0; i < size; i++) {
			ret[i] = (byte) ((getNetData().split("(?<=\\G.{4})"))[i].hashCode() % 256);
		}
		return ret;
	}

	private static String netData = null;

	private static String getNetData() {
		if (netData == null || netData.equals(null)) {
			Enumeration<NetworkInterface> nis = null;
			try {
				nis = NetworkInterface.getNetworkInterfaces();
			} catch (SocketException e) {
				e.printStackTrace();
			}
			if (nis == null) {
				netData = "A43s1ASDa-asda32=2=3fsf24aSADAmOP+-aEzx1ASDMS+sasdda0-a9aujsd0a-sad09as_ASASD-ad0-afkasf-KF_a0As-0d_J__oop51w912";
				return netData;
			}
			StringBuilder data = new StringBuilder();
			while (nis.hasMoreElements()) {
				NetworkInterface ni = nis.nextElement();
				data.append(ni.getName() + " " + ni.getDisplayName());
			}
			netData = data.toString();
		}
		return netData;
	}
}
