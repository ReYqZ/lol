package me.lpk.client.command.impl;

import java.util.ArrayList;

import me.lpk.client.Client;
import me.lpk.client.command.Command;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.Setting;
import me.lpk.client.module.data.SettingsMap;
import me.lpk.client.module.impl.other.ChatCommands;
import me.lpk.client.util.ChatUtil;
import me.lpk.client.util.StringConversions;
import net.minecraft.util.EnumChatFormatting;

public class Say extends Command {

	public Say(String[] names, String description) {
		super(names, description);
	}

	@Override
	public void fire(String[] args) {
		if (args == null) {
			return;
		}
		if (args.length > 0) {
			StringBuilder out = new StringBuilder();
			for (String word : args) {
				out.append(word + " ");
			}
			// TODO: Find a fix for sendChat(s) duplicating the message
			ChatUtil.sendChat_NoFilter(out.substring(0, out.length() - 1));
		}
	}

	@Override
	public String getUsage() {
		return "Say <Message>";
	}

}
