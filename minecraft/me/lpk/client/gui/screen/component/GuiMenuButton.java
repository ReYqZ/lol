package me.lpk.client.gui.screen.component;

import me.lpk.client.util.render.Colors;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;

public class GuiMenuButton extends GuiButton {

	public GuiMenuButton(int buttonId, int x, int y, int widthIn, int heightIn, String buttonText) {
		super(buttonId, x, y, widthIn, heightIn, buttonText);
	}

	public void drawButton(Minecraft mc, int mouseX, int mouseY) {
		if (this.visible) {
			boolean drawIDs = true;
			FontRenderer font = mc.fontRendererObj;
			this.hovered = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
			mouseDragged(mc, mouseX, mouseY);

			int text = hovered ? Colors.getColor(150, 150, 210, 100) : Colors.getColor(200, 100);
			int fill = hovered ? Colors.getColor(5, 20, 30, 50) : Colors.getColor(0, 100);
			int borderOut = Colors.getColor(55, 255);
			int borderIn = Colors.getColor(22, 255);
			Gui.drawBorderedRect(xPosition, yPosition, xPosition + width, yPosition + height, 1, borderOut);
			Gui.drawRect(xPosition + 1, yPosition + 1, xPosition + width-1, yPosition + height, fill);
			Gui.drawBorderedRect(xPosition + 1, yPosition + 1, xPosition + width-1, yPosition + height - 1, 1, borderIn);

			double scaleUp = 1.15;
			GlStateManager.pushMatrix();
			GlStateManager.scale(scaleUp, scaleUp, scaleUp);
			GlStateManager.translate((xPosition + width / 2) * (1 / scaleUp), (yPosition + 5) * (1 / scaleUp), 1);
			drawCenteredString(font, displayString, 0, 0, text);
			GlStateManager.popMatrix();
			if (drawIDs) {
				GlStateManager.pushMatrix();
				double ScaleDown = 0.8;
				GlStateManager.scale(ScaleDown, ScaleDown, ScaleDown);
				GlStateManager.translate((xPosition + 3) * (1 / ScaleDown), (yPosition + height - 9) * (1 / ScaleDown), 1);
				drawString(font, (id + 1) + ".", 0, 0, text);
				GlStateManager.popMatrix();
			}
		}
	}
}
