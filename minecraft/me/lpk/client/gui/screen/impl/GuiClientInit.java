package me.lpk.client.gui.screen.impl;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import me.lpk.client.Client;
import me.lpk.client.account.Account;
import me.lpk.client.gui.screen.PanoramaScreen;
import me.lpk.client.gui.screen.impl.mainmenu.ClientMainMenu;
import me.lpk.client.util.render.Colors;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.PasswordField;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.Session;

public class GuiClientInit extends PanoramaScreen {
	private GuiTextField usernameBox;
	private PasswordField passwordBox;
	private static String text;

	@Override
	public void initGui() {
		Minecraft mc = Minecraft.getMinecraft();
		// If the user has registered, skip this screen
		String username = mc.getSession().getUsername();
		Account account = Client.getAccountManager().getAccount(username);
		if (account != null) {
			Client.getAccountManager().setCurrent(account);
			if (mc.getSession().getSessionID().contains("token:0:")) {
				System.out.println("Logging in to account cuz bad session");
				mc.setSession(Session.loginPassword(account.getUser(), account.getPass(), mc.proxy));
			}
			mc.displayGuiScreen(new ClientMainMenu());
			return;
		}
		//
		int btnPosY = this.height / 4 + 64;
		int btnWidth = 200 / 3 + 2;
		int btnPosXLeft = this.width / 2 - (int) (btnWidth * (9.0f / 6.0f));
		int btnPosXMiddle = this.width / 2 - (int) (btnWidth / 2.0f);
		int btnPosXRight = this.width / 2 + (int) (btnWidth * (3.0f / 6.0f));
		int btnHeight = 24;
		buttonList.add(new GuiButton(0, btnPosXLeft, btnPosY + btnHeight, btnWidth, 20, "Register"));
		buttonList.add(new GuiButton(1, btnPosXMiddle, btnPosY + btnHeight, btnWidth, 20, "Exit"));
		buttonList.add(new GuiButton(2, btnPosXRight, btnPosY + btnHeight, btnWidth, 20, "Use Existing"));
		Keyboard.enableRepeatEvents(true);
		usernameBox = new GuiTextField(6, fontRendererObj, width / 2 - 100, 76, 200, 20);
		passwordBox = new PasswordField(fontRendererObj, width / 2 - 100, 116, 200, 20);
		usernameBox.setMaxStringLength(50);
		passwordBox.setMaxStringLength(50);
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if (button.id == 0) {
			Account account = new Account(usernameBox.getText().trim(), passwordBox.getText().length() > 0 ? passwordBox.getText().trim() : "");
			try {
				Session session = Session.loginPassword(account.getUser(), account.getPass(), mc.proxy);
				if (session != null) {
					mc.setSession(session);
					account.setDisplay(session.getUsername());
					Client.getAccountManager().add(account);
					mc.displayGuiScreen(new ClientMainMenu());
				}
			} catch (Exception e) {}
			text = "Error logging in. Please use a premium account";
		} else if (button.id == 1) {
			System.exit(0);
		} else if (button.id == 2) {
			mc.displayGuiScreen(new GuiAccountList(this));
		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
		drawString(fontRendererObj, "Username", width / 2 - 100, 63, 0xa0a0a0);
		drawString(fontRendererObj, "Password", width / 2 - 100, 104, 0xa0a0a0);
		drawString(fontRendererObj, text, width / 2 - fontRendererObj.getStringWidth(text) / 2, 174, Colors.getColor(200, 20, 20));
		try {
			usernameBox.drawTextBox();
			passwordBox.drawTextBox();
		} catch (final Exception err) {
			err.printStackTrace();
		}
	}

	@Override
	protected void keyTyped(char c, int i) {
		usernameBox.textboxKeyTyped(c, i);
		passwordBox.textboxKeyTyped(c, i);
		if (c == '\t') {
			if (usernameBox.isFocused()) {
				usernameBox.setFocused(false);
				passwordBox.setFocused(true);
			} else {
				usernameBox.setFocused(true);
				passwordBox.setFocused(false);
			}
		}
		if (c == '\r') {
			try {
				actionPerformed((GuiButton) buttonList.get(0));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void mouseClicked(int x, int y, int b) {
		usernameBox.mouseClicked(x, y, b);
		passwordBox.mouseClicked(x, y, b);
		try {
			super.mouseClicked(x, y, b);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	@Override
	public void updateScreen() {
		super.updateScreen();
		usernameBox.updateCursorCounter();
		passwordBox.updateCursorCounter();
	}
}
