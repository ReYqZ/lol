package me.lpk.client.gui.screen.impl.overlay;

import java.awt.Rectangle;
import java.io.IOException;
import java.util.ArrayList;

import me.lpk.client.Client;
import me.lpk.client.command.Command;
import me.lpk.client.gui.clickgui.component.impl.Panel;
import me.lpk.client.gui.clickgui.component.impl.info.CommandInfo;
import me.lpk.client.gui.clickgui.component.impl.info.ModuleInfo;
import me.lpk.client.gui.clickgui.component.impl.tabs.Tab;
import me.lpk.client.gui.clickgui.component.impl.tabs.TabbedPanel;
import me.lpk.client.gui.clickgui.renderer.GuiRenderer;
import me.lpk.client.gui.clickgui.renderer.impl.BasicRenderer;
import me.lpk.client.module.Module;
import me.lpk.client.module.impl.other.ChatCommands;
import me.lpk.client.util.render.Screen;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;

public class GuiHelpOverlay extends GuiScreen {
	private final TabbedPanel tabbed;

	public GuiHelpOverlay() {
		// Get the resolution
		ScaledResolution sr = Screen.getResolution();
		// Create the panel
		Rectangle rect = new Rectangle(0, 0, sr.getScaledWidth(), (int) (sr.getScaledHeight() * (7.0f / 10.0f)));
		tabbed = new TabbedPanel(rect, null);
		// Add tabs
		tabbed.addTab("Modules", getModulePanel(tabbed));
		tabbed.addTab("Commands", getCommandPanel(tabbed));
	}

	@Override
	public void mouseClicked(int x, int y, int mode) throws IOException {
		tabbed.onClick(x, y);
	}

	@Override
	protected void mouseReleased(int x, int y, int mode) {
		tabbed.onRelease(x, y);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		// Draw the TabbedPanel
		Client.getGuiTheme().drawComponent(tabbed, mouseX, mouseY);
		// Draw the text given by the TabbedPanel
		int y = tabbed.getArea().height + 10;
		int x = 5;
		for (String s : tabbed.getDisplayText()) {
			fontRendererObj.drawStringWithShadow(s, x, y, -1);
			y += 11;
		}
	}

	/**
	 * Generates a panel with organized buttons for all of the currently loaded
	 * modules.
	 * 
	 * @param tabbed
	 * @return
	 */
	private Panel getModulePanel(TabbedPanel tabbed) {
		// Get the resolution and create the base panel
		ScaledResolution sr = Screen.getResolution();
		Rectangle panelRect = new Rectangle(0, tabbed.getTabHeight(), (int) sr.getScaledWidth(), tabbed.getArea().height);
		Panel panel = new Panel(panelRect, tabbed);
		// Add the modules to the panel
		int x = 5, y = 5;
		for (Module module : Client.getModuleManager().getArray()) {
			int width = 80, height = 15;
			// If the module button will be off the panel, move it over and
			// reset the y position
			if (y + height >= panelRect.getHeight() - height) {
				y = 5;
				x += width + 5;
			}
			Rectangle moduleRect = new Rectangle(x, y, width, height);
			// Add the module information button
			ModuleInfo mi = new ModuleInfo(module, moduleRect, panel);
			panel.add(mi);
			y += height + 5;
		}
		return panel;
	}

	/**
	 * Generates a panel with organized buttons for all of the currently loaded
	 * commands.
	 * 
	 * @param tabbed
	 * @return
	 */
	private Panel getCommandPanel(TabbedPanel tabbed) {
		// Get the resolution and create the base panel
		ScaledResolution sr = Screen.getResolution();
		Rectangle panelRect = new Rectangle(0, tabbed.getTabHeight(), (int) sr.getScaledWidth(), tabbed.getArea().height - tabbed.getTabHeight());
		Panel panel = new Panel(panelRect, tabbed);
		int x = 5, y = 5;
		// Add the commands to the panel
		// Due to commands having being registered multiple times via aliases,
		// take only the first instance (Determined by the names in the alias
		// String[])
		ArrayList<String> used = new ArrayList<String>();
		for (Command command : ((ChatCommands) Client.getModuleManager().get(ChatCommands.class)).getCommands()) {
			if (used.contains(command.getName())) {
				continue;
			}
			used.add(command.getName());
			int width = 80, height = 15;
			// If the command button will be off the panel, move it over and
			// reset the y position
			if (y + height >= panelRect.getHeight() - height) {
				y = 5;
				x += width + 5;
			}
			Rectangle moduleRect = new Rectangle(x, y, width, height);
			// Add the command information button
			CommandInfo ci = new CommandInfo(command, moduleRect, panel);
			panel.add(ci);
			y += height + 5;
		}
		return panel;
	}

}