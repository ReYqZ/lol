package me.lpk.client.gui.clickgui.renderer;

import com.google.gson.annotations.Expose;

import me.lpk.client.gui.clickgui.component.Component;
import me.lpk.client.gui.clickgui.component.impl.Frame;
import me.lpk.client.gui.clickgui.component.impl.tabs.TabbedPanel;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;

public abstract class GuiRenderer {
	protected static final Minecraft mc = Minecraft.getMinecraft();
	/**
	 * Unused
	 */
	@Expose
	protected int headerWidth;
	/**
	 * The height of the grippable area of frames.
	 */
	@Expose
	protected int headerHeight;
	/**
	 * The height of frame objects.
	 */
	@Expose
	protected int objectHeight;
	/**
	 * The margin horizontal space between frame edge and component.
	 */
	@Expose
	protected int objectMarginVertical;
	/**
	 * The margin between each object vertically.
	 */
	@Expose
	protected int objectMarginHorizontal;
	/**
	 * The vertical padding between component's edges and internal content.
	 */
	@Expose
	protected int objectPaddingVertical;
	/**
	 * The horizontal padding between component's edges and internal content.
	 */
	@Expose
	protected int objectPaddingHorizontal;

	public final void drawComponent(Component component, int mouseX, int mouseY) {
		if (component instanceof Frame) {
			drawFrame((Frame) component, mouseX, mouseY);
		} else if (component instanceof TabbedPanel) {
			drawTabbedPanel((TabbedPanel) component, mouseX, mouseY);
		}
	}

	public void drawFrame(Frame frame, int mouseX, int mouseY) {}

	public void drawTabbedPanel(TabbedPanel tabbed, int mouseX, int mouseY) {}

	public abstract void setup();

	public void update(GuiRenderer newTheme) {
		headerWidth = newTheme.headerWidth;
		headerHeight = newTheme.headerHeight;
		objectHeight = newTheme.objectHeight;
		objectMarginVertical = newTheme.objectMarginVertical;
		objectMarginHorizontal = newTheme.objectMarginHorizontal;
		objectPaddingVertical = newTheme.objectPaddingVertical;
		objectPaddingHorizontal = newTheme.objectPaddingHorizontal;
	}

	public int getHeaderWidth() {
		return headerWidth;
	}

	public void setHeaderWidth(int headerWidth) {
		this.headerWidth = headerWidth;
	}

	public int getHeaderHeight() {
		return headerHeight;
	}

	public void setHeaderHeight(int headerHeight) {
		this.headerHeight = headerHeight;
	}

	public int getObjectHeight() {
		return objectHeight;
	}

	public void setObjectHeight(int objectHeight) {
		this.objectHeight = objectHeight;
	}

	public int getObjectMarginVertical() {
		return objectMarginVertical;
	}

	public void setObjectMarginVertical(int objectMarginVertical) {
		this.objectMarginVertical = objectMarginVertical;
	}

	public int getObjectMarginHorizontal() {
		return objectMarginHorizontal;
	}

	public void setObjectMarginHorizontal(int objectMarginHorizontal) {
		this.objectMarginHorizontal = objectMarginHorizontal;
	}

	public int getObjectPaddingVertical() {
		return objectPaddingVertical;
	}

	public void setObjectPaddingVertical(int objectPaddingVertical) {
		this.objectPaddingVertical = objectPaddingVertical;
	}

	public int getObjectPaddingHorizontal() {
		return objectPaddingHorizontal;
	}

	public void setObjectPaddingHorizontal(int objectPaddingHorizontal) {
		this.objectPaddingHorizontal = objectPaddingHorizontal;
	}
}
