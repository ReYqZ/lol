package me.lpk.client.gui.clickgui.component.impl.info;

import java.awt.Rectangle;

import me.lpk.client.gui.clickgui.component.Component;
import me.lpk.client.module.Module;

public class ModuleInfo extends Component {
	private final Module module;

	public ModuleInfo(Module module, Rectangle area, Component parent) {
		super(area, parent);
		this.module = module;
	}

	@Override
	public void onClick(int x, int y) {}

	@Override
	public void onRelease(int x, int y) {}

	public Module getModule() {
		return module;
	}
}
