package me.lpk.client.gui.clickgui.component.impl;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import me.lpk.client.gui.clickgui.component.Component;

public class Panel extends Component {
	protected ArrayList<Component> children = new ArrayList();

	public Panel(Rectangle area, Component parent) {
		super(area, parent);
	}

	@Override
	public void onClick(int x, int y) {

	}

	@Override
	public void onRelease(int x, int y) {

	}

	public List<Component> getChildren() {
		return this.children;
	}

	public void add(Component component) {
		this.children.add(component);
	}
}
