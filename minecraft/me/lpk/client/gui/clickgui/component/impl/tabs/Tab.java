package me.lpk.client.gui.clickgui.component.impl.tabs;

import java.awt.Rectangle;

import me.lpk.client.gui.clickgui.component.Component;

public class Tab extends Component {
	private boolean isSelected;
	private final String title;

	public Tab(String title,Rectangle area, Component parent) {
		super(area, parent);
		this.title = title;
	}

	@Override
	public void onClick(int x, int y) {}

	@Override
	public void onRelease(int x, int y) {}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public String getTitle() {
		return title;
	}
}
