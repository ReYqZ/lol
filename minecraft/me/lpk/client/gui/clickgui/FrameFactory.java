package me.lpk.client.gui.clickgui;

import java.awt.Rectangle;
import java.util.HashMap;

import me.lpk.client.Client;
import me.lpk.client.gui.clickgui.component.impl.Frame;
import me.lpk.client.gui.clickgui.component.impl.LinkedButton;
import me.lpk.client.gui.clickgui.renderer.GuiRenderer;
import me.lpk.client.gui.screen.impl.overlay.GuiClickOverlay;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import net.minecraft.client.Minecraft;

public class FrameFactory {
	public static Frame getFrameFromType(GuiClickOverlay screen, ModuleData.Type type, GuiRenderer renderer) {
		// Init width and height.
		int width = 90, height = renderer.getHeaderHeight();
		int lineHeight = renderer.getObjectHeight();
		// Use a hashmap to save the height locations of modules in the loop.
		HashMap<Module, Integer> modulePlaces = new HashMap<Module, Integer>();
		for (Module module : Client.instance.getModuleManager().getArray()) {
			// Skip
			if (module == null || module.getType() != type) {
				continue;
			}
			// Get the width of the would-be frame for the given module and
			// update the frame width if necessary.
			int nameWidth = Minecraft.getMinecraft().fontRendererObj.getStringWidth(module.getName());
			int lineWidth = nameWidth + (renderer.getObjectPaddingHorizontal() * 2) + (renderer.getObjectPaddingHorizontal() * 2);
			if (nameWidth > width) {
				width = lineWidth;
			}
			// Mark the place of the given module
			modulePlaces.put(module, height);
			// Update height
			height += lineHeight + (renderer.getObjectPaddingVertical() * 2) + (renderer.getObjectMarginVertical() * 2);
		}
		height -= renderer.getObjectPaddingVertical()*2;
		// Create the frame with the determined width and height.
		Frame frame = new Frame(screen, new Rectangle(0, 0, width, height), type.name());
		// Create and add module buttons to the frame
		for (Module module : modulePlaces.keySet()) {
			int x = renderer.getObjectPaddingHorizontal();
			int y = modulePlaces.get(module);
			int mWidth = width - (renderer.getObjectPaddingHorizontal() * 2);
			int mHeight = renderer.getObjectHeight();
			Rectangle area = new Rectangle(x, y, mWidth, mHeight);
			LinkedButton button = new LinkedButton(area, frame, module);
			frame.add(button);
			frame.setWidth(Math.max(width, mWidth));
		}
		return frame;
	}
}
