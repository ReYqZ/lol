package me.lpk.client.keybinding;

/**
 * An interface indicating that this object should receive specific-key inputs.
 */
public interface Bindable {
	void setKeybind(Keybind newBind);

	void onBindPress();

	void onBindRelease();
}
