package me.lpk.client.event.impl;

import me.lpk.client.event.Event;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;

public class EventAttack extends Event {
	private Entity entity;
	private boolean preAttack;

	public void fire(Entity targetEntity, boolean preAttack) {
		this.entity = targetEntity;
		this.preAttack = preAttack;
		super.fire();
	}

	public Entity getEntity() {
		return entity;
	}

	public boolean isPreAttack() {
		return preAttack;
	}

	public boolean isPostAttack() {
		return !preAttack;
	}
}
