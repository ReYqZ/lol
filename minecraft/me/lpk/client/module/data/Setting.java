package me.lpk.client.module.data;

import java.lang.reflect.Type;

import com.google.gson.annotations.Expose;

public class Setting<E> {
	private final String name;
	private final Type type;
	@Expose
	private E value;

	public Setting(String name, E value) {
		this.name = name;
		this.value = value;
		this.type = value.getClass().getGenericSuperclass();
	}

	public void setValue(E value) {
		this.value = value;
	}

	public E getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public Type getType() {
		return type;
	}

	public void update(Setting setting) {
		value = (E) setting.value;
	}
}
