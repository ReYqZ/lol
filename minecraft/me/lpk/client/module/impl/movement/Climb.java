package me.lpk.client.module.impl.movement;

import org.lwjgl.input.Keyboard;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;

public class Climb extends Module {
	private static final String KEY_SPEED = "SPEED";

	public Climb(ModuleData data) {
		super(data);
		settings.put(Climb.KEY_SPEED, new Setting(Climb.KEY_SPEED, 0.34));
	}

	@Override
	@RegisterEvent(events = { EventTick.class })
	public void onEvent(Event event) {
		if (mc.thePlayer.isCollidedHorizontally && mc.gameSettings.keyBindForward.getIsKeyPressed()) {
			mc.thePlayer.onGround = true;
			mc.thePlayer.motionY = (Double) settings.get(Climb.KEY_SPEED).getValue();
		}
	}
}
