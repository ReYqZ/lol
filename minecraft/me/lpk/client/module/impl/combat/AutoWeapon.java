package me.lpk.client.module.impl.combat;

import org.lwjgl.input.Keyboard;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventAttack;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;
import me.lpk.client.util.NetUtil;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.network.play.client.C09PacketHeldItemChange;

public class AutoWeapon extends Module {
	public AutoWeapon(ModuleData data) {
		super(data);
	}

	@Override
	@RegisterEvent(events = { EventAttack.class })
	public void onEvent(Event event) {
		//Check for only pre-attack events
		EventAttack ea = (EventAttack) event;
		if (ea.isPostAttack()) {
			return;
		}
		//Iterate through the hotbar and record the index of the best weapon
		int max = 9;
		int best = -1;
		float bestDam = -1;
		for (int i = 0; i < max; i++) {
			ItemStack itemStack = mc.thePlayer.inventory.mainInventory[i];
			if (itemStack == null) {
				continue;
			}
			if (itemStack.getItem() instanceof ItemSword) {
				ItemSword is = (ItemSword) itemStack.getItem();
				float damage = is.getDamageGiven();
				if (damage > bestDam) {
					bestDam = damage;
					best = i;
				}
			}
		}
		//If a weapon is found, make it the current item;
		if (best >= 0 && mc.thePlayer.inventory.currentItem != best) {
			mc.thePlayer.inventory.currentItem = best;
			NetUtil.sendPacketNoEvents(new C09PacketHeldItemChange(best));
		}
	}
}
