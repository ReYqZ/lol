package me.lpk.client.module.impl.combat;

import org.lwjgl.input.Keyboard;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventPacket;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.event.impl.EventVelocity;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;
import net.minecraft.network.play.server.S12PacketEntityVelocity;

public class AntiVelocity extends Module {
	private static final String KEY_PERCENT = "PERCENT-CUT";

	public AntiVelocity(ModuleData data) {
		super(data);
		settings.put(AntiVelocity.KEY_PERCENT, new Setting(AntiVelocity.KEY_PERCENT, 0.85));
	}

	@Override
	@RegisterEvent(events = { EventPacket.class })
	public void onEvent(Event event) {
		// Check for incoming packets only
		EventPacket ep = (EventPacket) event;
		if (ep.isOutgoing()) {
			return;
		}
		// If the packet handles velocity
		if (ep.getPacket() instanceof S12PacketEntityVelocity) {
			S12PacketEntityVelocity v = (S12PacketEntityVelocity) ep.getPacket();
			double percent = 1 - ((Number) settings.get(KEY_PERCENT).getValue()).doubleValue();
			// If the entity ID is the player's multiply the motion by a
			// percentage
			if (v.getEntityID() != mc.thePlayer.getEntityId()) {
				return;
			}
			v.setMotX((int) (v.getX() * percent));
			v.setMotY((int) (v.getY() * percent));
			v.setMotZ((int) (v.getZ() * percent));
		}
	}
}
