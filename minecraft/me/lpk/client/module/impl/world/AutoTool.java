package me.lpk.client.module.impl.world;

import org.lwjgl.input.Keyboard;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;
import me.lpk.client.util.BlockUtil;
import me.lpk.client.util.misc.Timer;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

public class AutoTool extends Module {
	public AutoTool(ModuleData data) {
		super(data);
	}

	@Override
	@RegisterEvent(events = { EventTick.class })
	public void onEvent(Event event) {
		if (!mc.gameSettings.keyBindAttack.getIsKeyPressed()) {
			return;
		}
		if (mc.objectMouseOver == null) {
			return;
		}
		BlockPos pos = mc.objectMouseOver.getBlockPos();
		if (pos == null) {
			return;
		}
		BlockUtil.updateTool(pos);
	}
}
