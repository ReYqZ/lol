package me.lpk.client.module.impl.world;

import org.lwjgl.input.Keyboard;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;
import me.lpk.client.util.misc.Timer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;

public class Fastmine extends Module {
	private static final String KEY_DELAY = "MINE-DELAY";
	private static final String KEY_HIT_DELAY = "NO-HIT-DELAY";
	private final Timer timer = new Timer();

	public Fastmine(ModuleData data) {
		super(data);
		settings.put(Fastmine.KEY_DELAY, new Setting(Fastmine.KEY_DELAY, 1));
		settings.put(Fastmine.KEY_HIT_DELAY, new Setting(Fastmine.KEY_HIT_DELAY, true));
	}

	@Override
	@RegisterEvent(events = { EventTick.class })
	public void onEvent(Event event) {
		float delay = ((Number) settings.get(Fastmine.KEY_DELAY).getValue()).floatValue();
		if (!mc.gameSettings.keyBindAttack.getIsKeyPressed()) {
			return;
		}
		if (mc.objectMouseOver == null) {
			return;
		}
		BlockPos pos = mc.objectMouseOver.getBlockPos();
		EnumFacing face = mc.objectMouseOver.facing;
		if (pos == null || face == null) {
			return;
		}
		if ((Boolean) settings.get(KEY_HIT_DELAY).getValue()) {
			mc.playerController.blockHitDelay = 0;
		}
		if (timer.check(delay)) {
			mc.playerController.breakBlock(pos, face);
			timer.reset();
		}
	}
}
