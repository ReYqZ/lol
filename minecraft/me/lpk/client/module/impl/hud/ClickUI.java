package me.lpk.client.module.impl.hud;

import org.lwjgl.input.Keyboard;

import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventTick;
import me.lpk.client.gui.screen.impl.overlay.GuiClickOverlay;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;

public class ClickUI extends Module {
	private GuiClickOverlay screen;

	public ClickUI(ModuleData data) {
		super(data);
	}

	@Override
	public void onEnable() {
		if (screen == null) {
			screen = new GuiClickOverlay();
		}
		mc.displayGuiScreen(screen);
		toggle();
	}

	@Override
	public void onEvent(Event event) {}
}
