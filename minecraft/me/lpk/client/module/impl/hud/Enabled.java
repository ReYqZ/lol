package me.lpk.client.module.impl.hud;

import org.lwjgl.input.Keyboard;

import com.google.common.collect.Lists;

import me.lpk.client.Client;
import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventRenderGui;
import me.lpk.client.keybinding.KeyMask;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.impl.other.ChatCommands;
import net.minecraft.client.renderer.GlStateManager;

public class Enabled extends Module {

	public Enabled(ModuleData data) {
		super(data);
	}

	@Override
	@RegisterEvent(events = { EventRenderGui.class })
	public void onEvent(Event event) {
		int y = 2;
		double scale = 1.5;
		GlStateManager.scale(1 / scale, 1 / scale, 1 / scale);
		for (Module module : Client.instance.getModuleManager().getArray()) {
			if (module.isEnabled() && !shouldHide(module)) {
				mc.fontRendererObj.drawString(module.getName(), 2, y, -1);
				y += 12;
			}
		}
		GlStateManager.scale(scale, scale, scale);
	}

	private boolean shouldHide(Module module) {
		ModuleData.Type type = module.getType();
		if (type == ModuleData.Type.HUD) {
			return true;
		}
		if (isBlacklisted(module.getClass())) {
			return true;
		}
		return false;
	}

	private boolean isBlacklisted(Class<? extends Module> clazz) {
		//TODO: Replace if statement with a way that lets users change the blacklist
		if (clazz.equals(ChatCommands.class)) {
			return true;
		}
		return false;
	}
}
