package me.lpk.client.module.impl.other;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import me.lpk.client.command.Command;
import me.lpk.client.command.impl.Bind;
import me.lpk.client.command.impl.Help;
import me.lpk.client.command.impl.Say;
import me.lpk.client.command.impl.Settings;
import me.lpk.client.event.Event;
import me.lpk.client.event.RegisterEvent;
import me.lpk.client.event.impl.EventChat;
import me.lpk.client.module.Module;
import me.lpk.client.module.data.ModuleData;
import me.lpk.client.module.data.Setting;
import me.lpk.client.module.impl.render.Xray;

public class ChatCommands extends Module {
	private static final String KEY_PREFIX = "CHAT-PREFIX";
	private final HashMap<String, Command> commandMap = new HashMap<String, Command>();

	public ChatCommands(ModuleData data) {
		super(data);
		settings.put(ChatCommands.KEY_PREFIX, new Setting(ChatCommands.KEY_PREFIX, "."));
		setup();
	}

	@Override
	@RegisterEvent(events = { EventChat.class })
	public void onEvent(Event event) {
		EventChat ec = (EventChat) event;
		//If the event does not start with the chat prefix, ignore it
		String prefix = (String) settings.get(KEY_PREFIX).getValue();
		if (!ec.getText().startsWith(prefix)) {
			return;
		}
		//If it begins with the chat prefix, cancel it.
		event.setCancelled(true);
		//Get the command and its arguments
		String commandBits[] = ec.getText().substring(prefix.length()).split(" ");
		String commandName = commandBits[0];
		//Get the command and fire it with arguments
		Command command = commandMap.get(commandName);
		if (command == null) {
			return;
		}
		if (commandBits.length > 1) {
			String[] commandArguments = Arrays.copyOfRange(commandBits, 1, commandBits.length);
			command.fire(commandArguments);
		} else {
			command.fire(null);
		}
	}

	/**
	 * Registers commands
	 */
	private void setup() {
		new Settings(new String[] { "Setting", "set", "s" }, "Changing and listing settings for modules.").register(this);
		new Help(new String[] { "Help", "halp", "h" }, "Help for commands.").register(this);
		new Say(new String[] { "Say", "talk", "chat" }, "Send a message with your chat prefix.").register(this);
		new Bind(new String[] { "Bind", "key", "b" }, "Send a message with your chat prefix.").register(this);
	}

	public void addCommand(String name, Command command) {
		commandMap.put(name, command);
	}

	public Collection<Command> getCommands() {
		return commandMap.values();
	}

	public Command getCommand(String name) {
		return commandMap.get(name.toLowerCase());
	}
}
